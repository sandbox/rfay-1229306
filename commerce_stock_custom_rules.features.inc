<?php
/**
 * @file
 * commerce_stock_custom_rules.features.inc
 */

/**
 * Implementation of hook_commerce_product_default_types().
 */
function commerce_stock_custom_rules_commerce_product_default_types() {
  $items = array(
    'product' => array(
      'type' => 'product',
      'name' => 'Product',
      'description' => 'A basic Product',
      'help' => '',
      'module' => 'commerce_product_ui',
    ),
  );
  return $items;
}
