<?php
/**
 * @file
 * commerce_stock_custom_rules.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function commerce_stock_custom_rules_default_rules_configuration() {
  $items = array();
  $items['rules_notify_when_reorder_threshold_re'] = entity_import('rules_config', '{ "rules_notify_when_reorder_threshold_re" : {
      "LABEL" : "Notify when reorder threshold reached",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_product_update" ],
      "IF" : [
        { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "commerce_stock" } },
        { "entity_has_field" : { "entity" : [ "commerce-product" ], "field" : "field_reorder_threshold" } },
        { "entity_has_field" : {
            "entity" : [ "commerce-product-unchanged" ],
            "field" : "commerce_stock"
          }
        },
        { "AND" : [
            { "data_is" : {
                "data" : [ "commerce-product:commerce-stock" ],
                "op" : "\\u003c",
                "value" : [ "commerce-product:field-reorder-threshold" ]
              }
            },
            { "NOT data_is" : {
                "data" : [ "commerce-product-unchanged:commerce-stock" ],
                "op" : "\\u003c",
                "value" : [ "commerce-product:field-reorder-threshold" ]
              }
            }
          ]
        }
      ],
      "DO" : [
        { "drupal_message" : { "message" : "Your product [commerce-product:title] has stock of [commerce-product:commerce-stock] which is below the reorder threshold of [commerce-product:field-reorder-threshold]. Order some more now!" } }
      ]
    }
  }');
  return $items;
}
